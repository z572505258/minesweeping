﻿using System.Windows.Forms;
namespace c_sharp
{

    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            int ClientW = 800, ClientH = 800;
            this.ClientSize = new System.Drawing.Size(ClientW, ClientH+30);

            start = new Button();
            start.Location = new System.Drawing.Point(10,0);
            start.Name = "start";
            start.Size = new System.Drawing.Size(60, 30);
            start.TabIndex = 0;
            start.Text = "开始";
            start.UseVisualStyleBackColor = true;
            start.Click += ClickStart;
            this.Controls.Add(start);


            end = new Button();
            end.Location = new System.Drawing.Point(10+70,0);
            end.Name = "start";
            end.Size = new System.Drawing.Size(100, 30);
            end.TabIndex = 0;
            end.Text = "我不玩了";
            end.UseVisualStyleBackColor = true;
            end.Click += ClickEnd;
            this.Controls.Add(end);

            check = new Button();
            check.Location = new System.Drawing.Point(10 + 70 + 110, 0);
            check.Name = "start";
            check.Size = new System.Drawing.Size(100, 30);
            check.TabIndex = 0;
            check.Text = "我完成了";
            check.UseVisualStyleBackColor = true;
            check.Click += CheckClick;
            this.Controls.Add(check);

            for (int i = 0; i < n; i++)
                for (int j = 0; j < m; j++)
                {
                    this.button[i, j] = new MyButton();
                    
                    this.button[i, j].i = i;
                    this.button[i, j].j = j;
                    this.button[i, j].Location = new System.Drawing.Point(j* ClientW / m, i * ClientH / n + 30);
                    this.button[i, j].Name = (i*n+ j).ToString();
                    this.button[i, j].Size = new System.Drawing.Size(ClientW / m, ClientH / n);
                    this.button[i, j].TabIndex = i+j;
                    //this.button[i, j].Text = i.ToString()+j.ToString();
                    this.button[i, j].Text = "";
                    this.button[i, j].UseVisualStyleBackColor = true;
                    this.button[i, j].Click += button_Click;
                    this.button[i, j].MouseDown += button_rClick;
                    this.Controls.Add(this.button[i, j]);

                    this.button[i, j].Enabled = false;
                }
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            
            this.Name = "MainForm";
            this.Text = "扫雷";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);

        }

        #endregion


        static private int n = 20,m=20;

        Button start, end,check;
        private MyButton[,] button = new MyButton[n,m];
    }



    public class MyButton : Button
    {
        
        public int sum ;
        public bool flag;
        public bool IsVised;
        public int i, j;
        public bool HasMine;
    }
}

