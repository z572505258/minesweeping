﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace c_sharp
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
        }

        private void ClickStart(object sender, EventArgs e)
        {
            this.FlagNum = 0;
            MineSum = n*m/6;
            for (int i = 0; i < n; i++)
                for (int j = 0; j < m; j++)
                {
                    this.button[i, j].Text = "";
                    this.button[i, j].sum = 0;
                    this.button[i, j].IsVised = false;
                    this.button[i, j].HasMine = false;
                    
                    this.button[i, j].flag = false;
                    this.button[i, j].Enabled = true;
                }

            Random rand = new Random();
            int l = MineSum;
            for(int i=0 ;i < l ; i++)
            {
                int ti = rand.Next()%n;
                int tj = rand.Next()%m;
                while (this.button[ti, tj].HasMine == true)
                {
                    ti = rand.Next() % n;
                    tj = rand.Next() % m;
                }
                this.button[ti, tj].HasMine = true;
                for (int k = 0; k < 8; k++)
                {
                    int ni = ti + mi[k], nj = tj + mj[k];
                    if (ni >= 0 && ni < n && nj >= 0 && nj < m)
                    {
                        this.button[ni, nj].sum++;
                    }
                }
            }
            
        }

        private void ClickEnd(object sender, EventArgs e)
        {
            for (int i = 0; i < n; i++)
                for (int j = 0; j < m; j++)
                {
                    if (this.button[i, j].HasMine == true) this.button[i, j].Text = "雷";
                    else this.button[i, j].Text = this.button[i, j].sum.ToString();
                    this.button[i, j].Enabled = false;
                }
        
        }

        int MineSum;
        int FlagNum;
        static int[] mi = new int[] { 1,-1,0, 0, -1,-1,1,1};
        static int[] mj = new int[] { 0, 0,1,-1 ,1, -1,-1,1 };
        private void button_Click(object sender, EventArgs e)
        {
            
            MyButton button = (MyButton)sender;

            if (button.flag == true||button.IsVised==true) return;
     
            if (button.HasMine == true)
            {
                button.Text = "雷";
                ClickEnd(null, null);
                MessageBox.Show("你输了！");
                return;
            }

            
            Queue<Point> que = new Queue<Point>();
            que.Enqueue(new Point(button.i,button.j));
            while (que.Any())
            {
                Point tmp = que.Dequeue();
                int i = tmp.X, j = tmp.Y;
                this.button[i,j].IsVised = true;
                this.button[i, j].Enabled = false;
                if (this.button[i, j].sum > 0)
                {
                    this.button[i, j].Text = this.button[i, j].sum.ToString();
                    continue;
                }
                //this.button[i, j].Enabled = false;
                for(int k=0;k<8;k++)
                {
                    int ti = i + mi[k], tj = j + mj[k];
                    if(ti>=0 && ti<n && tj>=0 && tj<m && this.button[ti,tj].IsVised==false)
                    {
                        que.Enqueue(new Point(ti, tj));
                    }
                }
                //que.Enqueue(button.Location);
            }

        }
        private void button_rClick(object sender, EventArgs e)
        {
            MyButton button = (MyButton)sender;
            MouseEventArgs m = (MouseEventArgs)e;
            if(m.Button== System.Windows.Forms.MouseButtons.Right)
            {
                if (button.flag == false)
                {
                    button.flag = true;
                    button.Text = "F";
                    this.FlagNum++;
                }
                else
                {
                    button.flag = false;;
                    button.Text = "";
                    this.FlagNum--;
                }
                //button.Enabled = false;
            }
        }

        private void CheckClick(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            if(FlagNum < MineSum)
            {
                MessageBox.Show("您的标记不够!");
            }
            else if (FlagNum > MineSum)
            {
                MessageBox.Show("您的标记太多了!");
            }
            else
            {
                ClickEnd(null, null);
                for(int i=0;i< n;i++)
                    for(int j=0;j< m; j++)
                    {
                        if(this.button[i,j].flag==true && this.button[i, j].HasMine== false)
                        {
                            MessageBox.Show("您输了！");
                            return;
                        }
                    }
                MessageBox.Show("恭喜您！");
            }
        }

    }
}
